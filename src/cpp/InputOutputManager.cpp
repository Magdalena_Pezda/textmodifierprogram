#include "..\h\InputOutputManager.h"

InputOutputManager::InputOutputManager()
{
}

InputOutputManager::~InputOutputManager()
{
}

bool InputOutputManager::uploadTextFile(string pathToFile)
{
	string fileText;
	file.open(pathToFile.c_str(), ios::in);

	if (file.good())
	{
		while (getline(file, fileText))
		{
			textFromFile.push_back(fileText);
		}
		file.close();
		return true;
	}
	else
	{
		cout << "Wrong file name!\n";
		return false;
	}
}

void InputOutputManager::uploadTextFromStandardInput()
{
	string userInput;
	cout << "If you want to finish writing press enter + q + enter\n";

	while (getline(cin, userInput))
	{
		if (userInput == "q")
			break;
		textFromFile.push_back(userInput);
	}
}

bool InputOutputManager::saveToFile(string pathToFile)
{
	file.open(pathToFile, ios::out);

	if (file.good() == true)
	{
		for (unsigned int i = 0; i < getNumberOfTextLines(); ++i)
		{
			file << textFromFile[i] << "\n";
		}
		file.close();
		return true;
	}
	else
	{
		return false;
	}
}

void InputOutputManager::printData()
{
	for (auto &it : textFromFile)
		cout << it << endl;
}

unsigned int InputOutputManager::getNumberOfTextLines()
{
	return textFromFile.size();
}
