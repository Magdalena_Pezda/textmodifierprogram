#include "..\h\TextModifier.h"

TextModifier::TextModifier(vector<string> text)
{
	this->text = text;
}

TextModifier::~TextModifier()
{
}

vector<string> TextModifier::replaceText(string toDelete, string toUpload, unsigned int startingLine, unsigned int endingLine)
{
	if (startingLine == 0 && endingLine == 0)
	{
		for (unsigned int i = 0; i < text.size(); ++i)
		{
			int searchStartingPossition = 0;
			while (text[i].find(toDelete, searchStartingPossition) != string::npos)
			{
				int position = text[i].find(toDelete, searchStartingPossition);
				char nextChar = text[i][position + toDelete.length()];
				char prevChar = text[i][position - 1];

				if (!isalpha(nextChar) && (position == 0 || (!isalpha(prevChar))))
				{
					text[i] = text[i].replace(position, toDelete.length(), toUpload);
					searchStartingPossition = position + toUpload.length();
				}
				else
					searchStartingPossition = position + toDelete.length();
			}
		}
	}
	else
	{
		for (unsigned int i = startingLine - 1; i < endingLine; ++i)
		{
			int searchStartingPossition = 0;
			while (text[i].find(toDelete, searchStartingPossition) != string::npos)
			{
				int position = text[i].find(toDelete, searchStartingPossition);
				char nextChar = text[i][position + toDelete.length()];
				char prevChar = text[i][position - 1];

				if (!isalpha(nextChar) && (position == 0 || (!isalpha(prevChar))))
				{
					text[i] = text[i].replace(position, toDelete.length(), toUpload);
					searchStartingPossition = position + toUpload.length();
				}
				else
					searchStartingPossition = position + toDelete.length();
			}
		}
	}
	return text;
}
