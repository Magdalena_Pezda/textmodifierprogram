#include "..\h\UserInteraction.h"

UserInteraction::UserInteraction()
{
}

UserInteraction::~UserInteraction()
{
}

bool UserInteraction::isUploadOptionValid(int userSelection)
{
	return !(userSelection > 2 || userSelection < 1);
}

int UserInteraction::selectionOfUploadText()
{
	int userSelection(0);

	cout << "Please choose option:\n"
			"1) Load text file\n"
			"2) Provide text using standard input\n";
	cin >> userSelection;

	if (cin.fail() || !isUploadOptionValid(userSelection))
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "Wrong number\n";
		return selectionOfUploadText();
	}
	return userSelection;
}

int UserInteraction::selectionOfSaveText()
{
	int userSelection(0);
	cout << "Please select option:\n"
			"1) Save as text file.\n"
			"2) Show text on monitor\n";
	cin >> userSelection;
	if (cin.fail() || !isUploadOptionValid(userSelection))
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "Wrong number!\n";
		return selectionOfSaveText();
	}
	return userSelection;
}

bool UserInteraction::isSelectionOfModificationValid(char userSelection)
{
	if (userSelection == 'y' || userSelection == 'n')
		return true;
	return false;
}

char UserInteraction::selectionOfModificationRange()
{
	string userSelection;

	cout << "Do you want to change word in whole text y/n? \n";
	cin >> userSelection;
	if(userSelection.length() == 1)
	{
		if (!isSelectionOfModificationValid(userSelection[0]))
		{
			cout << "Wrong key!\n";
			return selectionOfModificationRange();
		}
		else
			return userSelection[0];
	}
	else
	{
		cout << "Wrong key!\n";
		return selectionOfModificationRange();
	}
}

bool UserInteraction::isStartingLineValid(unsigned int startingLine, unsigned int numberOfTextLines)
{
	if (startingLine <= 0 || startingLine > numberOfTextLines)
		return false;
	return true;
}

unsigned int UserInteraction::getStartingLineForModification(unsigned int numberOfTextLines)
{
	unsigned int startingLine(0);
	cout << "Enter number of starting line and press enter\n";
	cin >> startingLine;
	if (cin.fail() || !(isStartingLineValid(startingLine, numberOfTextLines)))
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "Wrong line number!\n";
		return getStartingLineForModification(numberOfTextLines);
	}
	return startingLine;
}

bool UserInteraction::isEndingLineValid(unsigned int startingLine, unsigned int endingLine, unsigned int numberOfTextLines)
{
	if (endingLine <= 0 || endingLine > numberOfTextLines || endingLine < startingLine)
		return false;
	return true;
}

unsigned int UserInteraction::getEndingLineForModification(unsigned int startingLine, unsigned int numberOfTextLines)
{
	unsigned int endingLine(0);
	cout << "Enter number of ending line and press enter\n";
	cin >> endingLine;
	if (cin.fail() || !isEndingLineValid(startingLine, endingLine,numberOfTextLines))
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "Wrong line number!\n";
		return getEndingLineForModification(startingLine, numberOfTextLines);
	}
	return endingLine;
}
