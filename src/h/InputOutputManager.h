#ifndef INPUTOUTPUTMANAGER_H_
#define INPUTOUTPUTMANAGER_H_

#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

class InputOutputManager
{
public:
	InputOutputManager();
	virtual ~InputOutputManager();

	bool uploadTextFile(string pathToFile);
	void uploadTextFromStandardInput();
	bool saveToFile(string pathToFile);
	void printData();
	unsigned int getNumberOfTextLines();

	const vector<string> getVector() const
	{
		return textFromFile;
	}

	void setVector(vector<string> textFromFile)
	{
		this->textFromFile = textFromFile;
	}

private:
	vector<string> textFromFile;
	fstream file;
};

#endif /* INPUTOUTPUTMANAGER_H_ */
