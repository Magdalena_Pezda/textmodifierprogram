#ifndef TEXTMODIFIER_H_
#define TEXTMODIFIER_H_
#include <iostream>
#include "InputOutputManager.h"
using namespace std;

class TextModifier
{
public:
	TextModifier(vector<string> text);
	virtual ~TextModifier();

	vector<string> replaceText(string toDelete, string toUpload, unsigned int startingLine = 0, unsigned int endingLine = 0);

private:
	vector<string> text;
};

#endif /* TEXTMODIFIER_H_ */
