#ifndef USERINTERACTION_H_
#define USERINTERACTION_H_
#include <iostream>
#include <vector>
using namespace std;

class UserInteraction
{
public:
	UserInteraction();
	virtual ~UserInteraction();

	static bool isUploadOptionValid(int userSelection);
	static int selectionOfUploadText();
	static int selectionOfSaveText();
	static bool isSelectionOfModificationValid(char userSelection);
	static char selectionOfModificationRange();
	static bool isStartingLineValid(unsigned int startingLine, unsigned int numberOfTextLines);
	static unsigned int getStartingLineForModification(unsigned int numberOfTextLines);
	static bool isEndingLineValid(unsigned int startingLine, unsigned int endingLine, unsigned int numberOfTextLines);
	static unsigned int getEndingLineForModification(unsigned int startingLine, unsigned int numberOfTextLines);
};

#endif /* USERINTERACTION_H_ */
