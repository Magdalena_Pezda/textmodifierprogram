#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "h\InputOutputManager.h"
#include "h\TextModifier.h"
#include "h\UserInteraction.h"

using namespace std;

bool isNumber(string number);

int main(int argc, char* argv[])
{
	string oldWord("");
	string newWord("");
	string sourceFileName("");
	string destinationFileName("");
	string rangeBeginning("");
	string rangeEnd("");

	if (argc == 3)
	{
		oldWord = argv[1];
		newWord = argv[2];
	}
	else if (argc == 5)
	{
		oldWord = argv[1];
		newWord = argv[2];
		sourceFileName = argv[3];
		destinationFileName = argv[4];
	}
	else if (argc == 7)
	{
		oldWord = argv[1];
		newWord = argv[2];
		sourceFileName = argv[3];
		destinationFileName = argv[4];
		rangeBeginning = argv[5];
		rangeEnd = argv[6];
	}
	else
	{
		cout << "Please run program in appropriate way:\n";
		cout << "TextModifierProgram.exe <oldWord> <newWord> or\n";
		cout << "TextModifierProgram.exe <oldWord> <newWord> <sourceFile> <destinationFile> or\n";
		cout << "TextModifierProgram.exe <oldWord> <newWord> <sourceFile> <destinationFile> <startingLine> <endingLine>\n";
		return 0;
	}

	cout << "---------WELCOME TO THE TEXT EDITOR---------\n";

	InputOutputManager ioManager;

	int userSelection(0);

	if (!sourceFileName.empty())
	{
		if (!ioManager.uploadTextFile(sourceFileName))
		{
			cout << "Cannot load source file. Check path of source file.\n";
			return 0;
		}
	}
	else
	{
		userSelection = UserInteraction::selectionOfUploadText();

		switch (userSelection)
		{
		case 1:
		{
			bool resultOfUpload(true);
			do
			{
				cout << "Enter correct path to source file (absolute or relative):\n";
				cin >> sourceFileName;
				resultOfUpload = ioManager.uploadTextFile(sourceFileName);
			}
			while (!resultOfUpload);
		}
			break;

		case 2:
		{
			ioManager.uploadTextFromStandardInput();
		}
			break;
		}
	}

	vector<string> textToBeModified = ioManager.getVector();
	TextModifier textModifier(textToBeModified);

	if(rangeBeginning != "" && rangeEnd != "")
	{
		if(rangeBeginning=="0" && rangeEnd == "0")
			ioManager.setVector(textModifier.replaceText(oldWord,newWord));
		else
		{
			if(isNumber(rangeBeginning) && isNumber(rangeEnd))
			{
				unsigned int startingLine = atoi(rangeBeginning.c_str());
				unsigned int endingLine = atoi(rangeEnd.c_str());

				if(UserInteraction::isStartingLineValid(startingLine, ioManager.getNumberOfTextLines()) && UserInteraction::isEndingLineValid(startingLine, endingLine, ioManager.getNumberOfTextLines()))
				{
					ioManager.setVector(textModifier.replaceText(oldWord,newWord, startingLine, endingLine));
				}
				else
				{
					cout << "Wrong starting/ending line number!!\n";
					cout << "Please run program in appropriate way:\n";
					cout << "TextModifierProgram.exe <oldWord> <newWord> <sourceFile> <destinationFile> <startingLine> <endingLine>\n";
					return 0;
				}

			}
			else
			{
				cout << "Wrong starting/ending line number!!\n";
				cout << "Please run program in appropriate way:\n";
				cout << "TextModifierProgram.exe <oldWord> <newWord> <sourceFile> <destinationFile> <startingLine> <endingLine>\n";
				return 0;
			}
		}
	}
	else
	{
		switch (UserInteraction::selectionOfModificationRange())
		{
			case 'y':
			{
				ioManager.setVector(textModifier.replaceText(oldWord, newWord));
			}
				break;
			case 'n':
			{
				unsigned int startingLine =	UserInteraction::getStartingLineForModification(ioManager.getNumberOfTextLines());
				unsigned int endingLine = UserInteraction::getEndingLineForModification(startingLine, ioManager.getNumberOfTextLines());

				ioManager.setVector(textModifier.replaceText(oldWord, newWord, startingLine, endingLine));
			}
				break;
		}
	}

	if (!destinationFileName.empty())
	{
		if (!ioManager.saveToFile(destinationFileName))
		{
			cout << "Cannot save to provided file. Check path of destination file.\n";
			return 0;
		}
	}
	else
	{
		userSelection = UserInteraction::selectionOfSaveText();

		switch (userSelection)
		{
			case 1:
			{
				bool resultOfSave(true);
				do
				{
					cout << "Enter correct path to destination file (absolute or relative):\n";
					cin >> destinationFileName;
					resultOfSave = ioManager.saveToFile(destinationFileName);
				} while (!resultOfSave);
			}
				break;
			case 2:
			{
				ioManager.printData();
			}
				break;
		}
	}

	cout << "---------- TEXT HAS BEEN MODIFIED ----------\n";
	return 0;
}

bool isNumber(string number)
{
    char* p;
    strtol(number.c_str(), &p, 10);
    return *p == 0;
}
