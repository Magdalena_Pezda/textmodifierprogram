#include "InputOutputManagerOperationTestFixture.h"

TEST_F(InputOutoutManagerOperationTestFixture, numberOfTextLineCalledCorrectly)
{
	InputOutputManager ioManager;
	ioManager.setVector(testVector);
	EXPECT_EQ(10, ioManager.getNumberOfTextLines());
}
