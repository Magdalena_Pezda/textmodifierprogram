#include "TextModifierOperationTestFixture.h"

TEST_F(TextModifierOperationTestFixture, replaceTextCalledCorrectly)
{
	TextModifier modifier(testVector);
	vector<string> resultVector = modifier.replaceText("ala", "tomek");

	EXPECT_EQ("tomek ma kota", resultVector[0]);
	EXPECT_EQ("psa nie ma tomek", resultVector[1]);
	EXPECT_EQ("pies ma na imie alabaster", resultVector[2]);
	EXPECT_EQ("pies to nie koala", resultVector[3]);
	EXPECT_EQ("Ala nie ma psa", resultVector[4]);
	EXPECT_EQ("tomek! uwazaj na psa, tomek.", resultVector[5]);
	EXPECT_EQ("tomek@gmail.com", resultVector[6]);
}
