#ifndef TESTS_TEXTMODIFIEROPERATIONTESTFIXTURE_H_
#define TESTS_TEXTMODIFIEROPERATIONTESTFIXTURE_H_

#include "TextModifier.h"
#include <string>
#include "gtest.h"


class TextModifierOperationTestFixture : public testing::Test
{
public:
	TextModifierOperationTestFixture()
	{
	}
	virtual ~TextModifierOperationTestFixture()
	{
	}
	vector<string> testVector;

	void SetUp()
	{
		testVector.push_back("ala ma kota");
		testVector.push_back("psa nie ma ala");
		testVector.push_back("pies ma na imie alabaster");
		testVector.push_back("pies to nie koala");
		testVector.push_back("Ala nie ma psa");
		testVector.push_back("ala! uwazaj na psa, ala.");
		testVector.push_back("ala@gmail.com");
	}

	virtual void TearDown()
	{
	}
};

#endif /* TESTS_TEXTMODIFIEROPERATIONTESTFIXTURE_H_ */
