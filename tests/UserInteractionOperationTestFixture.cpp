#include "UserInteractionOperationTestFixture.h"

TEST_F(UserInteractionOperationTestFixture, isUploadOptionValidCalledCorrectly)
{
	EXPECT_TRUE(UserInteraction::isUploadOptionValid(1));
	EXPECT_TRUE(UserInteraction::isUploadOptionValid(2));
	EXPECT_FALSE(UserInteraction::isUploadOptionValid(-1));
	EXPECT_FALSE(UserInteraction::isUploadOptionValid(0));
	EXPECT_FALSE(UserInteraction::isUploadOptionValid(9));
	EXPECT_FALSE(UserInteraction::isUploadOptionValid(234));
}

TEST_F(UserInteractionOperationTestFixture, isSelectionOfModificationValidCalledCorrectly)
{
	UserInteraction userInteraction;
	EXPECT_TRUE(UserInteraction::isSelectionOfModificationValid('y'));
	EXPECT_TRUE(UserInteraction::isSelectionOfModificationValid('n'));
	EXPECT_FALSE(UserInteraction::isSelectionOfModificationValid('Y'));
	EXPECT_FALSE(UserInteraction::isSelectionOfModificationValid('N'));
	EXPECT_FALSE(UserInteraction::isSelectionOfModificationValid(0));
	EXPECT_FALSE(UserInteraction::isSelectionOfModificationValid('0'));
	EXPECT_FALSE(UserInteraction::isSelectionOfModificationValid('7'));
	EXPECT_FALSE(UserInteraction::isSelectionOfModificationValid('H'));
}

TEST_F(UserInteractionOperationTestFixture, isStartingLineValidCalledCorrectly)
{
	EXPECT_TRUE(UserInteraction::isStartingLineValid(1, testVector.size() ));
	EXPECT_TRUE(UserInteraction::isStartingLineValid(10, testVector.size()));
	EXPECT_FALSE(UserInteraction::isStartingLineValid(-10, testVector.size()));
	EXPECT_FALSE(UserInteraction::isStartingLineValid(0, testVector.size()));
	EXPECT_FALSE(UserInteraction::isStartingLineValid(11, testVector.size()));
	EXPECT_FALSE(UserInteraction::isStartingLineValid(156, testVector.size()));
}

TEST_F(UserInteractionOperationTestFixture, getEndingLineForModificationCalledCorrectly)
{
	EXPECT_TRUE(UserInteraction::isEndingLineValid(1, 10, testVector.size()));
	EXPECT_TRUE(UserInteraction::isEndingLineValid(4, 4, testVector.size()));
	EXPECT_TRUE(UserInteraction::isEndingLineValid(4, 7, testVector.size()));
	EXPECT_FALSE(UserInteraction::isEndingLineValid(3, -10, testVector.size()));
	EXPECT_FALSE(UserInteraction::isEndingLineValid(3, 0, testVector.size()));
	EXPECT_FALSE(UserInteraction::isEndingLineValid(3, 15, testVector.size()));
	EXPECT_FALSE(UserInteraction::isEndingLineValid(3, 1, testVector.size()));
}

