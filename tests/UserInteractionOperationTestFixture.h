#ifndef TESTS_USERINTERACTIONOPERATIONTESTFIXTURE_H_
#define TESTS_USERINTERACTIONOPERATIONTESTFIXTURE_H_

#include "UserInteraction.h"
#include "gtest.h"
#include <vector>
#include <string>

class UserInteractionOperationTestFixture: public testing::Test {
protected:

	UserInteractionOperationTestFixture()
	{
	};
	virtual ~UserInteractionOperationTestFixture()
	{
	};

	vector<string> testVector;

	void SetUp()
	{
		for (auto i = 0; i < 10; ++i)
			testVector.push_back("testing");
	}

	virtual void TearDown()
	{
	}
};

#endif /* TESTS_USERINTERACTIONOPERATIONTESTFIXTURE_H_ */
